import React from "react";
import "./styles.scss";

export default function BlankLayout({ children }) {
  return <div className="blank-playout">{children}</div>;
}
